-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 21-11-2018 a las 05:06:13
-- Versión del servidor: 10.1.36-MariaDB
-- Versión de PHP: 7.2.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `libreria`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `libros`
--

CREATE TABLE `libros` (
  `ID_Libro` int(11) DEFAULT NULL,
  `Titulo` varchar(40) NOT NULL,
  `Tipo` varchar(30) NOT NULL,
  `Editorial` varchar(20) NOT NULL,
  `Precio` decimal(5,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `libros`
--

INSERT INTO `libros` (`ID_Libro`, `Titulo`, `Tipo`, `Editorial`, `Precio`) VALUES
(NULL, 'Campesino', 'Poesia', 'Unica', '25.00'),
(NULL, 'hola', '', 'to', '35.00'),
(NULL, 'don quijote', 'nulo', 'ninguno', '10.00');
COMMIT;

DELIMITER $$
--
-- Procedimientos
--
CREATE DEFINER=`root`@`localhost` PROCEDURE `ActualizaPrecio` (IN `editorial` VARCHAR(20))  UPDATE libros SET Precio=Precio+(Precio*0.10) WHERE Editorial = trim(editorial)$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `ActualizaPrecio2` (IN `editorial` VARCHAR(50), IN `incremento` INT(10))  UPDATE libros SET Precio=(Precio+incremento)+Precio WHERE Editorial=trim(editorial)$$

DELIMITER ;



