<?php 

if(isset($_POST['btnEnviar']))
{
	$Titulo = $_POST['Titulo'];
	$Tipo = $_POST['Tipo'];
	$Editorial = $_POST['Editorial'];
	$Precio = $_POST['Precio'];

	require('abrirConexion.php');

	$query = "INSERT INTO libros (Titulo, Tipo, Editorial, Precio) 
				VALUES ('$Titulo', '$Tipo', '$Editorial', $Precio)";

	$resultado = mysqli_query($conexion, $query);

	if ($resultado) {
		echo '<script>alert("CREADO CORRECTAMENTE")</script>';
		echo '<script>location.href= \'index.php\'</script>';
	}else{
		echo '<script>alert("ERROR AL CREAR EL PRODUCTO")</script>';
	}
	require('cerrarConexion.php');
}

?>

<!DOCTYPE html>
<html>
<head>
	<title></title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css" integrity="sha384-/Y6pD6FV/Vv2HJnA6t+vslU6fwYXjCFtcEpHbNJ0lyAFsXTsjBbfaDjzALeQsN6M" crossorigin="anonymous">
</head>
<body>
	<div class="row">
	<div class="col-md-4"></div>
	<div class="col-md-4">
	
	<center><h1>Agregar Libro</h1><br/><br/></center>
	<form method="POST" >

	<div class="form-group">
		<label for="Titulo">Titulo del Libro</label>
		<input type="text" name="Titulo" class="form-control" id="Titulo" required>
	</div>

	<div class="form-group">
		<label for="Tipo">Autor del Libro</label>
		<input type="text" name="Tipo" class="form-control" id="Tipo" required>
	</div>

	<div class="form-group">
		<label for="Editorial">Editorial del Libro</label>
		<input type="text" name="Editorial" class="form-control" id="Editorial" required>
	</div>

	<div class="form-group">
		<label for="Precio">Precio del Libro</label>
		<input type="number" name="Precio" class="form-control" id="Precio" required>
	</div>

	<center>
		<input type="submit" name="btnEnviar" value="Crear" class="btn btn-success">
		<a class="btn btn-danger" href="index.php" role="button">Regresar</a>
	</center>
	
	</form>
</div>
</div>
</body>
</html>
</body>
</HTML>